module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a+b

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 1 = 1
hw1_2 n = (1 / fromIntegral (n*n)) + hw1_2(n-1)

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n = if  n>0 
          then  n * fact2 (n-2)
          else 1
-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isPrime :: Integer -> Bool
isPrime 1 = False
isPrime p = primeSh p (round $ fromIntegral(p)/2)

primeSh p 1 = True
primeSh p 0 = True
primeSh p a = if (modch p a)==0 
              then False
              else primeSh p (a-1)
modch a b = a `mod` b

-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b = if b<a
               then 0
               else prS a b
prS a b | a==b =primeInt a
        | a/=b = primeInt a + prS (a+1) b
primeInt a = if isPrime a
             then a
             else 0
