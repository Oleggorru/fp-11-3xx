
-- Список целых
data IntList = EmptyIntList             -- пустой
             | IntList { headInt :: Int -- голова
                       , tailInt :: IntList
                         -- хвост
                       }

-- НО: нам нужны список символов, список списков целых...

-- Чтобы DRY:
-- параметризованные типы
data List item = Empty          -- пустой
               | NonEmpty item (List item) -- голова и хвост
--                              ^-- использование списка

intList :: List Int
intList = NonEmpty 5 (NonEmpty 4 (NonEmpty 3 Empty))

{-
data [a] = []
         | a : [a]
-}

intList2 :: [Int]
intList2 = 5 : (4 : (3 : []))

intList3 :: [Int]
intList3 = [5,4,3]

length' :: [a] -> Int
length' [] = 0
length' (x:xs) = 1 + length' xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' pred [] = []
filter' pred (a:as) | pred a = a : filter' pred as
                    | otherwise = filter' pred as
-- (if pred a then (a:) else id) $ filter' pred as

(+++) :: [a] -> [a] -> [a]
[]     +++ bs = bs
(a:as) +++ bs = a : (as +++ bs)

map' :: (a -> b) -> [a] -> [b]
map' f [] = []
map' f (a:as) = f a : map' f as

foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' f b [] = b
foldl' f b (a:as) = foldl' f (f b a) as

--   foldl' f b0 [a0,a1,a2] =
-- = foldl' f (f b0 a0) [a1,a2] =
-- = foldl' f (f (f b0 a0) a1) [a2] =
-- = foldl' f (f (f (f b0 a0) a1) a2) [] =
-- = f (f (f b0 a0) a1) a2

data Maybe' a = Nothing'
              | Just' a

fromMaybe' :: a -> Maybe' a -> a
fromMaybe' def ma = case ma of
  Nothing' -> def
  Just' a -> a

fromMaybe'' :: a -> Maybe' a -> a
fromMaybe'' def Nothing' = def
fromMaybe'' _ (Just' a) = a

maybe' :: b -> (a -> b) -> Maybe' a -> b
maybe' b _ Nothing' = b
maybe' _ f (Just' a) = f a

catMaybes' :: [Maybe' a] -> [a]
catMaybes' [] = []
catMaybes' (Nothing' : mas) = catMaybes' mas
catMaybes' (Just' a  : mas) = a : catMaybes' mas

